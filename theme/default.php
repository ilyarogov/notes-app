<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
  <html>
    <head>
      <meta charset="utf-8">
      <title>Small Task List</title>
      <!--[if lt IE 9]>
              <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <base href="<?php echo BASE_URL ?>" />
      <link rel="stylesheet" type="text/css" href="web/assets/css/base.css" media="screen" />
      <link rel="stylesheet" type="text/css" href="web/assets/css/layout.css" media="screen" />
      <link rel="stylesheet" type="text/css" href="web/assets/css/skeleton.css" media="screen" />
      <link rel="stylesheet" type="text/css" href="web/assets/css/main.css" media="screen" />
      <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
      <script src="web/assets/js/notifications.js"></script>
    </head>
    <body>
      <div class="container">
        <?php echo $this->view->showMenu() ?>
        <?php if($this->isAuthenticated() === true): ?>
          <div class="welcome"><?php echo "Welcome " . $_SESSION['username'] ?> 
            <a href="logout">Logout</a>
          </div>
        <?php endif; ?>
        <?php $this->notice->getMessage() ?>
        <?php echo $this->content ?>
      </div>
    <div id="pwb">Powered by the <a href="http://getskeleton.com/">skeleton</a> framework</div>
  </body>
</html>