function displayNotification(type){
  var notification = $('.'+type);
  if(notification.length > 0){
    var errorLength = notification.text().length * 5;
    notification.fadeIn('slow');
    notification.css('width', errorLength+'px');
  }
}
$(document).ready(function(){
  displayNotification('error');
  displayNotification('notice');
});
