function submitNewItem(uid){
  $('body').delegate('.add-submit','click',function(){
    var title = $('#task-title').val();
    var cont = $('#task-content').val();
    var url = $('.add-submit').attr('action')+'/ms/'+new Date().getTime();
    $('#loader').show();
    $('#new-item-load').hide('slow');
    $.post(url, {
      itemTitle:title, 
      itemContent:cont, 
      user: uid
    }, function(data){
      $('#loader').hide();
      respData = $.parseJSON(data);
      if(respData.isValid == 1){
        $('.add-item-link').show(function(){
          $('.all-items').before('<div id="list-loader">Reloading list...</div>');
          $('.all-items').load(window.location.href +' .all-items');
          $('#list-loader').hide('slow');
          $('#list-loader').remove();
        });
                
      }else{
        alert('Please enter a title and body.');
        $('#new-item-load').show('slow');
      }
    });
  });
}

function addItemClickHandler(){
  $('.add-item-link').click(function(event){
    event.preventDefault();
    $('#loader').show();
    var url = $(this).attr('action')+'/ms/'+new Date().getTime();
    $('#new-item-load').load(url, function(){
      $('#loader').hide();
      $('#new-item-load').show('slow');
      $('.add-item-link').hide(); 
    });
  });
}

function viewImageClickHandler() {
  $('.view-image-link').click(function(event){
    event.preventDefault();
    var url = $(this).attr('action');
    var itemId = $(this).attr('itemId');
    $('#img-'+itemId).html("<img src="+url+"/>");
  });
}
function cancelAddItemClickHandler(){
  $('body').delegate('#cancel-add-submit','click',function(event){
    event.preventDefault();
    $('#add-submit').remove();
    $('.add-item-link').show(); 
  });
  $('body').delegate('#cancel-image-upload','click',function(event){
    event.preventDefault();
    $('.upload-form').remove();
    $('.add-item-link').show(); 
  });
}

function editItemClickHandler(){
  $('.all-items').delegate('.edit-item-link','click',function(event){
    event.preventDefault();
    var itemId = this.parentNode.parentNode.parentNode.id;
    var itemEl = $('#'+itemId);
    var itemTitle = itemEl.find('#item-title').text();
    var itemContent = itemEl.find('#item-content').text();
    var url = $(this).attr('action')+'/ms/'+new Date().getTime();
    var editItem = itemEl.find('.edit-item');
    var viewItem = itemEl.find('.view-item');
    viewItem.fadeOut('fast');
    //editItem.parent().find('.change-item').fadeOut('slow');
    if(editItem.html().length <= 0){
      editItem.append('<img src="web/assets/images/ajax-loader.gif"/>');
      editItem.load(url, function(){
        $(this).find('#task-title').val(itemTitle);
        $(this).find('#task-content').val(itemContent);
        submitEditItem(editItem, itemId, viewItem);   
      });
    }else{
      editItem.show();
      submitEditItem(editItem, itemId, viewItem);
    }
       
    editItem.delegate('#cancel-edit-submit','click',function(event){
      event.preventDefault();
      viewItem.show();
      editItem.hide();
    });
        
  });
}
function submitEditItem(editItem, itemId, viewItem){
  $('#'+itemId).delegate('.edit-submit','click',function(event){
    event.preventDefault();
    var newTitle = editItem.find('#task-title').val();    
    var newContent = editItem.find('#task-content').val();
    var url = $('.edit-submit').attr('action')+'/ms/'+new Date().getTime();
    $.post(url, {
      'itemId':itemId, 
      'itemTitle':newTitle, 
      'itemContent':newContent
    }, function(data){
      editItem.html(data);
      viewItem.show();
      $('#'+itemId).undelegate();
      $('#'+itemId).load(location.href+'?ms='+new Date().getTime() +' #'+itemId+'>*');
    });
  });
    
}
function deleteItemClickHandler(){
  $('.all-items').delegate('.delete-item-link','click',function(event){
    event.preventDefault();
    var conf = confirm('Are you sure you want to delete this item?');
    if(conf){
      var url = $(this).attr('action')+'/ms/'+new Date().getTime();
      var itemId = this.parentNode.parentNode.parentNode.id;
      $('.list-item#'+itemId).css('background-color', '#FF3030');
      $(this).parent().remove();
      $.post(url, {
        'itemId':itemId
      }, function(){
        $('.list-item#'+itemId).fadeOut('slow', function(){
          $(this).remove();
        });
      });
    }
  });
}

function clickItemShowOptions(){
  $('.all-items').delegate('.click-to-edit','click',function(event){
    $(this).parent().parent().find('.item-options').fadeToggle('slow');
    $(this).parent().parent().toggleClass('selected-item');
  });
}