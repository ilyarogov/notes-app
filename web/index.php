<?php
define('ROOT', dirname(dirname(__FILE__)).'/');
require_once ROOT . 'classes/Initializer.php';

try{
  new Initializer(filter_input(INPUT_GET, 'act', FILTER_SANITIZE_SPECIAL_CHARS));
}  catch (ErrorException $e){
  echo $e->getMessage();
}