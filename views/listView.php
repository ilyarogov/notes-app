<script src="web/assets/js/task-list.js"></script>
<script>
    var uid = '<?php $_SESSION['user_id']?>';
    $(document).ready(function(){
        $('#loader').hide();
        $('.list-item:odd').addClass('odd');
        $('.list-item:even').addClass('even');
        $('.click-to-edit').hover(
          function(){$(this).addClass('bold');},
          function(){$(this).removeClass('bold');}
        );
        clickItemShowOptions();
        //Adding a new list item
        addItemClickHandler();
        submitNewItem(uid);
        cancelAddItemClickHandler();
        //Editing existing list item
        editItemClickHandler();
        deleteItemClickHandler();
        viewImageClickHandler();
    });
</script>
<h2>All List Items</h2>
<button class="add-item-link" action="<?php echo BASE_URL.'item/new'?>">Add New Item</button>
<button class="add-item-link" action="<?php echo BASE_URL.'item/image'?>">Add Image</button>
<img id="loader" src="web/assets/images/ajax-loader.gif">
<div id="new-item-load"></div>
<div class="all-items">
    <?php foreach ($fullList as $singleListItem): ?>
        <div class="list-item" id="<?php echo $singleListItem->id ?>">
            <div class="view-item">
                <div id="item-added"><?php echo $singleListItem->added ?></div>
                <?php if($singleListItem->type_id == 1): ?>
                  <div id="item-title"><?php echo $singleListItem->title ?></div>
                  <div class="item-content"><?php echo $singleListItem->content ?></div>
                  <div id="item-led"><?php echo $singleListItem->last_edited ?></div>
                <?php else: ?>  
                  <div class="item-content" id="img-<?php echo $singleListItem->id ?>"><?php echo Accessories::getThumbnailImage($_SESSION['user_id'],$singleListItem->content) ?></div>
                <?php endif ?>  
                <div class="click-to-edit">options</div>
            </div>
            <div class="edit-item"></div>
            <ul class="item-options">
              <li>
                <?php if($singleListItem->type_id == 1): ?>
                  <a href="#" class="edit-item-link" action="<?php echo BASE_URL.'item/edit'?>">Edit Item</a>
                <?php else: ?>  
                  <a href="#" itemId="<?php echo $singleListItem->id ?>" class="view-image-link" action="<?php echo BASE_URL.'image/view:'.$singleListItem->id ?>">View Full Image</a>
                <?php endif ?> 
              </li>
              <li>
                <a href="#" class="delete-item-link" action="<?php echo BASE_URL.'item/delete'?>">Delete Item</a>
              </li>            
            </ul>
            <hr />
        </div>
    <?php endforeach; ?>
</div>

