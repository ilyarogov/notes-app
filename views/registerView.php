<div class="doc-section clearfix reglog-form" >
  <h4>Sign Up for an Account:</h4>
    <form name="input" action="" method="post">
        <div class="twelve columns alpha">
            <label id="username">Username (Your Email Address): <input type="text" name="username" /></label>
        </div>
        <div class="six columns omega">
            <label>Password: <input type="password" name="password" /></label>
        </div>
        <div class="six columns alpha">
            <label>Repeat Password: <input type="password" name="password2" /></label>
        </div>
        <input style="display: none" id="honeypot" type="text" name="0duyw8y82hf"/>

        <div class="six columns omega">
            <input type="submit" value="Register" />
        </div>
    </form>
</div>