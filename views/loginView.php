<div class="doc-section clearfix reglog-form" id="grid">
  <form method ="post" action="<?php echo BASE_URL.'login' ?>">
      <div class="twelve columns alpha">
          <h4>Please Log In or <a id="register-link" href="<?php echo BASE_URL.'register'?>"> Register</a>:</h4>
      </div>
      <div class="six columns omega">
          <label>Email: <input name="username" type="text" id="myusername"></label>
      </div>
      <div class="six columns alpha">
          <label>Password: <input name="password" type="password" id="mypassword"></label>
      </div>
      <div class="six columns omega">
          <input class="redButton" type="submit" name="Submit" value="Login"/>
      </div>
  </form>
</div>