<?php

class Controller extends BaseController
{
  public function register()
  {
    $this->content = $this->view->load('registerView');

    if($this->requestMethod == 'POST'){
      $db = dbConnect::getInstance();
      $name = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_EMAIL);
      $pass = trim(filter_input(INPUT_POST, 'password'));
      $pass2 = trim(filter_input(INPUT_POST, 'password2'));
      $honeypot = filter_input(INPUT_POST, '0duyw8y82hf');
      try {
        $user = new User($db, $name, $pass);
        $register = new Registration($db, $user, $pass2, $honeypot);
        $register->attach(new UserEmailer($db));
        $register->addUser($user);
        $_SESSION['messages'] = array('notice'=>'You have been successfully registered. Please check your email for a confirmation.');
        header('Location: ' . BASE_URL . 'home');
        die();
      } catch (UserException $e) {
        $_SESSION['messages'] = array('error'=>$e->getMessage());
        header('Location: ' . BASE_URL . 'register');
        die();
      }
    }
    $this->render();
  }
  
  public function login()
  {
    $db = dbConnect::getInstance();
    $name = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_EMAIL);
    $pass = trim(filter_input(INPUT_POST, 'password'));

    $this->user = new User($db, $name, $pass);

    try {
      $this->user->login();
      header('Location: ' . BASE_URL . 'home');
    } catch (UserException $e) {
      $_SESSION['messages'] = array('error' => $e->getMessage());
      header('Location: ' . BASE_URL . 'home');
    }
  }

  public function logout()
  {
    session_destroy();
    header('Location: ' . BASE_URL);
  }

  public function home()
  {
    if ($this->isAuthenticated()) {
      $db = dbConnect::getInstance();
      $taskList = new Notes($db);
      $userListItems = $taskList->viewUserListItems($_SESSION['user_id']);
      $this->content = $this->view->load('listView', array('fullList' => $userListItems));
    }else{
        $this->content = $this->view->load('loginView');
    }
    $this->render();
  }

  public function newItem()
  {
    if ($this->isAuthenticated()) {
      if ($this->requestMethod == 'POST') {
        $db = dbConnect::getInstance();
        $listItem = new Notes($db);
        $userId = (int) $_SESSION['user_id'];
        (int) trim(filter_input(INPUT_POST, 'itemId'));
        $title = Accessories::filterXss(filter_input(INPUT_POST, 'itemTitle'));
        $content = $this->contentIsUrl('itemContent');
        $listItem->addItem($userId, $title, $content);
      } else {
        $form_action = 'add-submit';
        echo $this->view->load('ajax/itemForm', array('form_action' => $form_action, 'req'=> 'item/new'));
      }
    }else{
        $this->content = $this->view->load('loginView');
    }
  }

  public function editItem()
  {
    if ($this->isAuthenticated()) {
      if ($this->requestMethod == 'POST') {
        $db = DbConnect::getInstance();
        $listItem = new Notes($db);
        $itemId = (int) trim(filter_input(INPUT_POST, 'itemId', FILTER_VALIDATE_INT));
        $userId = (int) $_SESSION['user_id'];
        $title = Accessories::filterXss(filter_input(INPUT_POST, 'itemTitle'));
        $content = $this->contentIsUrl('itemContent');
        $listItem->editItem($itemId, $userId, $content, $title);
        echo $listItem->getMessage();
      } else {
        $form_action = 'edit-submit';
        echo $this->view->load('ajax/itemForm', array('form_action' => $form_action, 'req'=>'item/edit'));
      }
    }else{
        $this->content = $this->view->load('loginView');
    }
  }

  public function deleteItem()
  {
    if ($this->isAuthenticated() && $this->requestMethod == 'POST') {
      $db = dbConnect::getInstance();
      $listItem = new Notes($db);
      $itemId = (int) trim(filter_input(INPUT_POST, 'itemId', FILTER_VALIDATE_INT));
      $listItem->deleteItem($itemId);
    }else{
        $this->content = $this->view->load('loginView');
    }
  }
  
  public function addImage() {
    if ($this->isAuthenticated()) {
       if ($this->requestMethod == 'POST') {
        $db = dbConnect::getInstance();
        $listItem = new Notes($db);
        $itemId = (int) trim(filter_input(INPUT_POST, 'itemId', FILTER_VALIDATE_INT));
        $userId = (int) $_SESSION['user_id'];
        try{
          $listItem->addImage($itemId, $userId, $_FILES);
        }catch(Exception $e){
          $_SESSION['messages'] = array('error'=>$e->getMessage());
          header('Location: ' . BASE_URL . 'home');
          die();
        }
        header('Location: ' . BASE_URL . 'home');
      } else {
        $form_action = 'Upload Image';
        echo $this->view->load('ajax/imageForm', array('form_action' => $form_action, 'req'=>'item/image'));
      }
    }else{
        $this->content = $this->view->load('loginView');
    }
  }
  
  public function viewImage($imageId)
  {
    if ($this->isAuthenticated()) {
      $uploadDir = "../../../uploads/".$_SESSION['user_id']."/";
      $db = DbConnect::getInstance();
      $listItem = new Notes($db);
      $image = $listItem->getImage($imageId);
      $imageFile = $uploadDir.$image['content'];
      $imageInfo = getimagesize($imageFile);
      header('Content-type: '.$imageInfo['mime']);
      readfile($imageFile);      
    }else{
      header('Location: ' . BASE_URL . 'home');
    }
  }
  
}