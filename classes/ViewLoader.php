<?php

class ViewLoader
{
  public function load($filename, $data = null)
  {
    if(is_array($data)){
      extract($data);
    }
    $viewName = $filename.'.php';
    $filepath = ROOT.'views/'.$viewName;
    if(file_exists($filepath)){
      ob_start();
      require $filepath;
      return ob_get_clean();
    }else{
      throw new ErrorException('The view: '.$viewName.' could not be found.');
    }
  }
  
  public function showMenu($flag = true)
  {
    if($flag){
      ob_start();
      require ROOT.'/theme/topMenu.php';
      return ob_get_clean();
    }else{
        return '';
    }
  }
}