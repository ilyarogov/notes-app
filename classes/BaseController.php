<?php

class BaseController
{

  protected $content;
  protected $view;
  protected $user;
  protected $requestMethod;

  public function __construct(viewLoader $view)
  {
    $this->requestMethod = $_SERVER['REQUEST_METHOD'];
    $this->view = $view;
  }

  public function render($theme = 'default') {
    $this->notice = new Notifier();
    require_once(ROOT . 'theme/' . $theme . '.php');
  }

  public function __call($method, $args) {
    if (!method_exists($this, $method)) {
      throw new ErrorException('Action ' . $method . ' does not exitst');
    }
  }

  protected function contentIsUrl($cont){
      if($ic = filter_input(INPUT_POST, $cont, FILTER_VALIDATE_URL)){
        $content = Accessories::formatUrl($ic);
      }else{$content = Accessories::filterXss(filter_input(INPUT_POST, $cont));}
      
      return $content;
  }

  protected function isAuthenticated() {
    //if user is authenticated, display the appropriate content
    return ($_SESSION['username']) ? true : false;
  }

}