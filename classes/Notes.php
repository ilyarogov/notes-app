<?php

class Notes
{
  private $message;
  private $db;
  
  public function __construct(PDO $db)
  {
    $this->db = $db;
  }
  
  public function addItem($argUserId, $argTitle, $argContent)
  {
    if (trim($argTitle) && trim($argContent)) {
      $db = $this->db;
      $type = 1;
      $listItemStmt = $db->prepare("INSERT INTO lists (user_id, type_id, title, content) 
            VALUES(:user_id, :type_id, :title, :content)");
      $listItemStmt->bindParam(':user_id', $argUserId);
      $listItemStmt->bindParam(':type_id', $type);
      $listItemStmt->bindParam(':title', $argTitle);
      $listItemStmt->bindParam(':content', $argContent);
      try {
        $listItemStmt->execute();
        $id = $db->lastInsertId();
        echo json_encode(array('isValid' => 1, 'id' => $id));
      } catch (PDOException $e) {
        $this->message = "Database Connection Error!: " . $e->getMessage() . "<br/>";
        echo json_encode(array('isValid' => 0));
        die();
      }
      $db = null;
    } else {
      echo json_encode(array('isValid' => 0));
    }
  }

  public function editItem($argItemId, $argUserId, $argContent, $argTitle)
  {
    $db = $this->db;
    $listItemStmt = $db->prepare("UPDATE lists SET title = :title, content = :content, last_edited = CURDATE()
                                      WHERE id=:item_id AND user_id=:user_id");
    $listItemStmt->bindParam(':title', $argTitle);
    $listItemStmt->bindParam(':content', $argContent);
    $listItemStmt->bindParam(':item_id', $argItemId);
    $listItemStmt->bindParam(':user_id', $argUserId);

    try {
      $listItemStmt->execute();
      $this->message = 'Item Successfully Edited';
    } catch (PDOException $e) {
      $this->message = "Database Connection Error!: " . $e->getMessage() . "<br/>";
      die($this->message);
    }
    $db = null;
  }

  public function getItem($argItemId)
  {
    $db = $this->db;
    $listItemStmt = $db->prepare("SELECT id, title, content, added 
            FROM lists WHERE id = :id LIMIT 1");
    try {
      $listItemStmt->execute(array(':id' => $argItemId));
      $item = $listItemStmt->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
      $this->message = "Database Connection Error!: " . $e->getMessage() . "<br/>";
      die();
    }
    $db = null;
    return $item;
  }

  public function deleteItem($argItemId)
  {
    $db = $this->db;
    $delStmt = $db->prepare('DELETE FROM lists WHERE id = :id')->execute(array(':id' => $argItemId));
  }

  public function viewUserListItems($argUserId)
  {
    $db = $this->db;
    $allItemList = array();
    $listItemStmt = $db->prepare("SELECT id, type_id, title, content, added 
            FROM lists WHERE user_id = :user_id
            ORDER BY id DESC");
    try {
      $listItemStmt->execute(array(':user_id' => $argUserId));
      while ($listItem = $listItemStmt->fetch(PDO::FETCH_OBJ)) {
        $fDate = new DateTime($listItem->added);
        $listItem->added = $fDate->format('F j, Y');
        $allItemList[] = $listItem;
      }
    } catch (PDOException $e) {
      $this->message = "Database Connection Error!: " . $e->getMessage() . "<br/>";
      die();
    }
    $db = null;
    return $allItemList;
  }
  
  public function addImage($argItemId, $argUserId, $fileInfo)
  {
    $type = 2;
    $image = new images($argUserId,$fileInfo);
    $image->createThumbnail();
    $db = $this->db;
    $listItemStmt = $db->prepare("INSERT INTO lists (user_id, type_id, content) 
            VALUES(:user_id, :type_id, :content)");
    $listItemStmt->bindParam(':user_id', $argUserId);
    $listItemStmt->bindParam(':type_id', $type);
    $listItemStmt->bindParam(':content', $image->getImageName());
    try {
      $listItemStmt->execute();
      $id = $db->lastInsertId();
      if($id == 0){
        throw new Exception("Image upload error. Please try again.");
      }
    } catch (PDOException $e) {
      $this->message = "Database Connection Error!: " . $e->getMessage() . "<br/>";
      echo json_encode(array('isValid' => 0));
      die();
    }
  }
  
  public function getImage($argItemId)
  {
    $db = $this->db;
    $listItemStmt = $db->prepare("SELECT content
            FROM lists WHERE id = :id LIMIT 1");
    try {
      $listItemStmt->execute(array(':id' => $argItemId));
      $item = $listItemStmt->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
      $this->message = "Database Connection Error!: " . $e->getMessage() . "<br/>";
      die();
    }
    $db = null;
    return $item;
  }

  public function getMessage() {
    return $this->message;
  }

}
