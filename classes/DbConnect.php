<?php

class DbConnect
{
  public static function getInstance()
  {
    try {
      $dbh = new PDO('mysql:host=localhost;dbname=' . $_SERVER["DBNAME"], $_SERVER["DBUSER"], $_SERVER["DBPASS"]);
      return $dbh;
    } catch (PDOException $e) {
      echo "Database Connection Error!: " . $e->getMessage() . "<br/>";
      die();
    }
  }

}