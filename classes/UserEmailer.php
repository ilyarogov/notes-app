<?php
class UserEmailer implements SplObserver
{
  private $db;
  private $address;
  
  public function __construct($db) {
    $this->db = $db;
  }

  public function update(SplSubject $subject) {
    if($subject->getObsMessage() == 'new user registered')
    {
      $this->getUserEmailAddress($subject->getUserId());
      $this->sendEmail();
    }
  }
  
  private function getUserEmailAddress($userId)
  {
    $emailStmt = $this->db->prepare("SELECT name FROM users WHERE user_id = :id");
    $emailStmt->execute(array(':id' => $userId));
    $result = $emailStmt->fetch(PDO::FETCH_OBJ);
    $this->address = $result->name;
  }
  
  private function sendEmail()
  {
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: DoNotReply@ilyarogov.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
    $message ='                 
          <html>
          <head>
            <title>Ilya Rogov\'s Task List</title>
          </head>
          <body>
            <p>Dear '.$this->address.', </p>
            <p> Thank you for registering. I hope you enjoy this application.
            <p>If you have any question feel free to email me at ilyarogov12@gmail.com </p>
            <p>To login, please visit <a href="http://www.ilyarogov.com/tasklist/">Tasklist Home</a>.</p>
          </body>
          </html>

';
    mail($this->address, 'Task List Registration Success', wordwrap($message), $headers);
  }
}