<?php
class RequestHandler
{
  public static $result;
  public static $params;

  public static function delegate($request)
  {
    $view = new ViewLoader();
    $controller = new Controller($view);
    self::filter($request);
    $action = self::$result;
    $rmap = self::getReqMap();

    $method = $rmap[$action];
    $method = $method == null ? '': $method;
    
    return $controller->$method(self::$params);
  }

  private static function getReqMap(){
      $requestMap = array();
      $requestMap[''] = 'home';
      $requestMap['home'] = 'home';
      $requestMap['register'] = 'register';
      $requestMap['login'] = 'login';
      $requestMap['logout'] = 'logout';
      $requestMap['item/new'] = 'newItem';
      $requestMap['item/edit'] = 'editItem';
      $requestMap['item/delete'] = 'deleteItem';
      $requestMap['item/image'] = 'addImage';
      $requestMap['image/view'] = 'viewImage';

      return $requestMap;
  }
  
  private static function filter($request)
  {
    $requestUri = explode('/', $request);
    $item =  $requestUri[0];
    $action = $requestUri[1];
    if(strpos($action, ":") > 0){
      $splitLastPart = explode(":",$action);
      self::$params = end($splitLastPart);
      $action = prev($splitLastPart);
    }
    self::$result = $action == null ?$item :$item.'/'.$action;
  }
}
