<?php

class User
{
  private $id = '';
  private $name = '';
  private $password = '';
  private $db;

  public function __construct(PDO $db, $argUsername, $argPassword)
  {
    $this->db = $db;
    $this->name = $argUsername;
    $this->password = sha1($argPassword);
  }

  public function login()
  {
    $db = $this->db;
    $loginCheck = $db->prepare("SELECT user_id, name, password FROM users 
            WHERE name = :name AND password = :password LIMIT 1");
    $loginCheck->execute(array(':name' => $this->name, ':password' => $this->password));
    $rows = $loginCheck->rowCount();
    $user = $loginCheck->fetch(PDO::FETCH_OBJ);
    $this->id = $user->user_id;
    if ($rows == 1) {
      $_SESSION['username'] = $this->name;
      $_SESSION['user_id'] = $this->id;
    } else {
      throw new UserException('Username or password incorrect', 1);
    }

    $loginCheck->closeCursor();
  }

  public function getUsername()
  {
    return $this->name;
  }
  
  public function getPassword()
  {
    return $this->password;
  }

  public function getId()
  {
    return $this->id;
  }

}
