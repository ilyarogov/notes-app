<?php

class Images
{
  private $fileInfo;
  private $newImageName;
  private $uploadPath;
  private $extension;
  
  private $thumbHeight;
  private $thumbWidth;
  
  public function __construct($userId, $file)
  {
    $this->fileInfo = $file;
    try{
      $this->validateFile();
    }  catch (Exception $e){
       throw new Exception("Image upload error. Please try again.");
    }
    $fileName = $this->fileInfo["file"]["name"];
    $pathInfo = pathinfo($fileName);
    $this->extension = strtolower($pathInfo['extension']);
    $tmpFile = $this->fileInfo["file"]["tmp_name"];
    $newFileName = md5(md5_file($tmpFile).uniqid());
    $this->newImageName = $newFileName.".".$this->extension;
    $this->uploadPath =  $this->makeDestDir($userId);
    move_uploaded_file($tmpFile, $this->uploadPath.$this->newImageName);
  }

  private function makeDestDir($userId)
  {
    $root = "../../../uploads/";
    $destDir = $root.$userId."/";
    if(!is_dir($destDir)){
      if(mkdir($destDir)){
        return $destDir;
      }else{
        throw new Exception("Could not create destination directory for image.");
      }
    }else{
      return $destDir;
    }
    
  }

  private function validateFile()
  {
    if ((($this->fileInfo["file"]["type"] == "image/gif")
    || ($this->fileInfo["file"]["type"] == "image/jpeg")
    || ($this->fileInfo["file"]["type"] == "image/png")
    || ($this->fileInfo["file"]["type"] == "image/pjpeg"))
    && ($this->fileInfo["file"]["size"] < 5*1000*1000)){
      if ($this->fileInfo["file"]["error"] > 0){
        throw new Exception("Image Upload Error: Return Code: " . $this->fileInfo["file"]["error"]);
      }
    }
    else{
      throw new Exception("Invalid file");
    }
  }
  
  public function createThumbnail()
  {
    $source = $this->uploadPath.$this->newImageName;
    $imageDims = getimagesize($source);
    $width = $imageDims[0];
    $height = $imageDims[1];

    if($this->extension == 'jpg' || $this->extension == 'jpeg'){
      $sourceImg = imagecreatefromjpeg($source);
    }
    if($this->extension == 'gif'){
      $sourceImg = imagecreatefromgif($source);
    }
    if($this->extension == 'png'){
      $sourceImg = imagecreatefrompng($source);
    }

    $this->setThumbnailDimenstions($width, $height, 150);
    $thumbWidth = $this->thumbWidth;
    $thumbHeight = $this->thumbHeight;
    
    $virtImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
    imagecopyresampled($virtImage, $sourceImg, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $width, $height);
    
    if($this->extension == 'jpg' || $this->extension == 'jpeg'){
      imagejpeg($virtImage, $this->uploadPath.'t_'.$this->newImageName);
    }
    if($this->extension == 'gif'){
      imagegif($virtImage, $this->uploadPath.'t_'.$this->newImageName);
    }
    if($this->extension == 'png'){
      imagepng($virtImage, $this->uploadPath.'t_'.$this->newImageName);
    }
  }
  
  private function setThumbnailDimenstions($width, $height, $target)
  {
    if ($width > $height) {
      $percentage = ($target / $width);
    } else {
      $percentage = ($target / $height);
    }
    $this->thumbWidth = round($width * $percentage);
    $this->thumbHeight = round($height * $percentage);
  }
  
  public function getImageName(){
    return $this->newImageName;
  }
  
  public function getImageLocation(){
    return $this->uploadPath;
  }
}