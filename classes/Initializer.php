<?php
class Initializer
{
  public function __construct($action)
  {
    session_start();
    $this->loadClasses();
    define('BASE_URL' , Accessories::getBaseUrl()); 
    RequestHandler::delegate($action);
  }
  
  private function loadClasses()
  {
    spl_autoload_register(
        function($className){
            $classFile = ROOT.'classes/'.$className.'.php';
            if(file_exists($classFile)) {
                require($classFile);
            }else{
                throw new ErrorException('Unable to load ' . $className);
            }
        }
    );
  }

}
