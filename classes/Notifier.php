<?php

class Notifier{

  private $messages;

  public function __construct()
  {
    $this->messages = $_SESSION['messages'];
  }

  public function getMessage()
  {
    if (is_array($this->messages)) {
      foreach ($this->messages as $type => $message) {
        $capType = strtoupper($type);
        echo "<div class='$type' style='display: none;'> 
                <span style='font-weight: bold;'>$capType:</span> $message</div>";
      }
      unset($_SESSION['messages']);
    }
  }

}