<?php
class Registration implements SplSubject
{
    private $db;
    private $observers = array();
    private $obsMessage;

    public function attach(SplObserver $observer) {
        $observerId = spl_object_hash($observer);
        $this->observers[$observerId] = $observer;
    }

    public function detach(SplObserver $observer) {
        unset($this->observers[spl_object_hash($observer)]);
    }

    public function notify() {
        foreach($this->observers as $observer){
            $observer->update($this);
        }
    }

    public function __construct(PDO $db, User $user, $pass2, $honeypot) {
        if($this->validateRegistration($user, $pass2, $honeypot) == true){
            $this->db = $db;
        }
    }

    public function addUser(User $user) {
        if ($this->usernameTaken($user->getUsername()) == false) {
            $addUserStmt = $this->db->prepare("INSERT INTO users (name, password, registered, ip_address) 
                VALUES (:name, :password, :registered, :ip_address)");
            $addUserStmt->bindParam(':name', $user->getUsername());
            $addUserStmt->bindParam(':password', $user->getPassword());
            $addUserStmt->bindParam(':registered', time());
            $addUserStmt->bindParam(':ip_address', $_SERVER['REMOTE_ADDR']);
            $addUserStmt->execute();
            $this->setObsMessage('new user registered');
            $this->notify($this);
        } else {
            throw new UserException('Username has been taken.', 5);
        }
    }

    private function validateRegistration(User $user, $pass2, $hp)
    {
        $pass = $user->getPassword();
        $username = $user->getUsername();
        $pass2 = sha1($pass2);
        if(is_string($username)){
            //May need to do additional password validation
            if ($pass == $pass2 && $pass != ''&& $pass2 != ''){
                if(Accessories::validEmail($user->getUsername()) && !$hp){
                    return true;
                } else {throw new UserException('Email is not valid', 3);}
            } else {throw new UserException('Password is not valid', 1);}
        }else{throw new UserException('Username is not valid', 2);}
    }

    public function getUserId()
    {
        $id = $this->db->lastInsertId();
        $this->db = null;

        return $id;
    }

    private function usernameTaken($name) {
        try {
            $userCheck = $this->db->prepare("SELECT name FROM users WHERE name = :name");
            $userCheck->execute(array(':name' => $name));

            $rows = $userCheck->rowCount();
            $userCheck->closeCursor();
            if ($rows > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo "Database Connection Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        $this->db = null;
    }

    private function setObsMessage($message)
    {
        $this->obsMessage = $message;
    }

    public function getObsMessage()
    {
        return $this->obsMessage;
    }
}